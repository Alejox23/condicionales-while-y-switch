package com.mycompany.team;
import java.util.Scanner;
/**
 * @author Wilmer Ramirez
 */
public class NewMain {
    public static void main(String[] args) {
       sumaC opsuma;
       restaC opresta;
       multiplicacionC opmultiplicacion; 
       divicionC opdivicion;
       int ejec=1;
        while (ejec==1){
        Scanner Entrada=new Scanner(System.in);
            System.out.println("Ingresa la operacion a realizar");
            System.out.println("1. suma");
            System.out.println("2. resta");
            System.out.println("3. multiplicacion");
            System.out.println("4. divicion");
        int a=Entrada.nextInt();
        switch(a){
            case 1:
                System.out.println("Ingresa el primer sumando");
                int s1=Entrada.nextInt();
                System.out.println("Ingresa el segundo sumando");
                int s2=Entrada.nextInt();
                System.out.println("Ingresa el tercer sumando");
                int s3=Entrada.nextInt();
                opsuma = new sumaC(s1,s2,s3);
                System.out.println("Su resultado es:");
                opsuma.imprimir();
                break;
            case 2:
                System.out.println("Ingresa el minuendo");
                int m=Entrada.nextInt();
                System.out.println("Ingresa el sustraendo");
                int s=Entrada.nextInt();
                opresta = new restaC(m,s);
                System.out.println("Su resultado es:");
                opresta.imprimir();
                break;
            case 3:
                System.out.println("Ingresa el multipicando");
                int mm=Entrada.nextInt();
                System.out.println("Ingresa el multiplicador");
                int M=Entrada.nextInt();
                opmultiplicacion = new multiplicacionC(mm,M);
                System.out.println("Su resultado es:");
                opmultiplicacion.imprimir();
                break;
            case 4:
                System.out.println("Ingresa el dividendo");
                int d=Entrada.nextInt();
                System.out.println("Ingresa el divisor");
                int D=Entrada.nextInt();
                opdivicion = new divicionC(d,D);
                System.out.println("Su resultado es:");
                opdivicion.imprimir();
                break;
            default:
                System.out.println("No se ingreso la opcion correcta");
            }
            System.out.println("Desea continuar haciendo operaciones:");
            System.out.println("1. continuar:");
            System.out.println("0. salir:");
            ejec=Entrada.nextInt();
        }    
        System.out.println("Gracias por usar este proyecto de calculadora");
    }
}

    
 
    